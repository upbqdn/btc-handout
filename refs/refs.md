# References

[bitcoin paper](www.bitcoin.org/bitcoin.pdf)

[zero-knowledge](http://zerocash-project.org/media/pdf/zerocash-extended-20140518.pdf)

[zero-knowledge](https://eprint.iacr.org/2013/879.pdf)

[zero-knowledge](https://www.ieee-security.org/TC/SP2015/papers-archived/6949a287.pdf)

[bitcoin explanation
article](http://www.michaelnielsen.org/ddi/how-the-bitcoin-protocol-actually-works/)

[bitcoin explanation video](https://www.youtube.com/watch?v=bBC-nXj3Ng4)

[bitcoin protocol](https://en.bitcoin.it/wiki/Protocol_documentation)

[PoS](https://bitcoinmagazine.com/articles/what-proof-of-stake-is-and-why-it-matters-1377531463/)

[Smart Contracts](https://en.bitcoin.it/wiki/Contract)

[Smart
Contracst](https://github.com/ethereum/wiki/wiki/White-Paper/f18902f4e7fb21dc92b37e8a0963eec4b3f4793a)

[blockchain demo video](https://www.youtube.com/watch?v=_160oMzblY8)

[blockchain demo video part 2](https://www.youtube.com/watch?v=xIDL_akeras)

[LN](https://lightning.network/lightning-network-paper.pdf)

[transactions](https://en.bitcoin.it/wiki/Transaction)
